const pkg = require('./package.json');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const libraryName = pkg.name;

module.exports = {
    entry : './app/index.js',
    output: {
        path: path.join(__dirname , 'dist'),
        filename: 'Button.js',      
        library: libraryName,      
        libraryTarget: 'umd',      
        publicPath: '/dist/',      
        umdNamedDefine: true
    },
    module: {
        rules: [
            {test: /\.(js)$/, use:'babel-loader'},
            {
                test: /\.css$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            esModule: true,
                        },
                    },
                    'css-loader',
                ],
            },
        ]
    },
    resolve: {      
        alias: {          
            'react': path.resolve(__dirname, './node_modules/react'),
            'react-dom': path.resolve(__dirname, './node_modules/react-dom')
        }  
    },
    externals: {        
        react: {          
            commonjs: "react",          
            commonjs2: "react",          
            amd: "React",          
            root: "React"      
        },      
        "react-dom": {          
            commonjs: "react-dom",          
            commonjs2: "react-dom",          
            amd: "ReactDOM",          
            root: "ReactDOM"      
        }  
    },
    mode:'development',
    plugins: [
        new HtmlWebpackPlugin({
            template: 'app/index.html'
        }),
        new MiniCssExtractPlugin({
            filename: 'button.css',
        })
    ]

}