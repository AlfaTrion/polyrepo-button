import React from 'react';
import ReactDOM from 'react-dom';
import Button from "../Button";
import './index.css';

const App = () => (
    <section>
    	<h2>Button variations</h2>

    	<Button copy="Abstract" />

    	<Button copy="Using '...rest'" role="button" className="myClass" data-rest="true" />

    	<Button copy="User Styled" className="demo" />
    </section>
)

ReactDOM.render(<App />, document.getElementById('app'));