//import React from "react";
import PropTypes from "prop-types";
import "./button.css";

const Button = ({ className, copy, ...rest }) => {
	const componentClass = "polyrepo-button";
	className = className ? `${className} ${componentClass}` : componentClass;

    return (
        <button className={className} {...rest}>
        	{copy}
        </button>
    );
};

Button.displayName = '@polyrepo/Button';

Button.propTypes = {
    copy: PropTypes.string.isRequired,
};

export default Button;
